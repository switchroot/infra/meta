# How to Contribute

Pull/Merge Requests are welcome! We ask a few things of contributors:
- Test your code before uploading
- Provide a succinct and properly formatted commit message and description
- Most discussion happens on our Discord servers, so if comments left are not sufficient or warrant further discussion, please join the servers (https://discord.gg/N9PPYXjWMY for Switchroot, https://discord.gg/53mtKYt for L4S)
- Please include proper copyright comments and proper attribution for any ports, picks, and squashes

Thank you!

- The Switchroot Team
