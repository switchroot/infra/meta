# Switchroot Project

Switchroot is a group for open-source development on the Nintendo Switch, focused on baremetal, low-level and kernel development.
We are committed to open-source development, and our projects are governed under our [LICENSE](LICENSE). You can also find our
work at [NX-Development](https://github.com/NX-Development/), [LineageOS](https://github.com/LineageOS) (check the Nintendo
projects), [Switchroot Staging](https://gitlab.thomasmak.in/switchroot-staging), and [L4T-Community](https://gitlab.azka.li/l4t-community).

Main site: https://switchroot.org/

Wiki: https://wiki.switchroot.org/

Downloads: https://download.switchroot.org/

Thank you to [GitBook](https://www.gitbook.com/) for sponsoring our wiki hosting!
